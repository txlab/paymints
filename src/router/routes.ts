import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/auth',
    component: () => import('pages/Auth.vue'),
  },

  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        components: {
          default: () => import('pages/Overview.vue'),
          header: () => import('pages/header/OverviewHeader.vue'),
        },
      },
      {
        path: 'add/:type',
        props: true,
        components: {
          default: () => import('pages/Add.vue'),
          header: () => import('pages/header/AddHeader.vue'),
        },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
]

export default routes
