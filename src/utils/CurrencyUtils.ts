import currency from 'currency.js'
import _ from 'lodash'

// Library docs: https://currency.js.org/
// We create a custom constructor with custom config here
export type Currency = currency

// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/unbound-method
const defaultFormatFunc = currency('42').s.format as ((currency: any, settings: any) => any)

const DefaultOptions: currency.Options = {
  errorOnInvalid: true,
  symbol: '€',
  decimal: ',',
  separator: ' ', // small space - https://de.wikipedia.org/wiki/Schreibweise_von_Zahlen#Internationale_Standards
  pattern: '! #',
  negativePattern: '- ! #',
  increment: 1.0, // round up to full

  format (c, options) {
    if (c === undefined) throw new Error('currency is undefined')
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return defaultFormatFunc(c, { ...this, ...options }).replace(/,00$/, '')
  },
}

// eslint-disable-next-line no-redeclare
export const Currency = function Currency (
  this: void | (() => currency),
  value: null | undefined | currency.Any,
  options: currency.Options = DefaultOptions,
  // @ts-expect-error
): currency {
  if (value === null || value === undefined) throw new Error(`Invalid currency input: ${value}`) // I don't like that it returns 0.00

  if (!(this instanceof Currency)) { // this enables calling the function without 'new'
    return new (Currency as any)(value, options) // eslint-disable-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-return
  }

  // Call the 'constructor' of currency.js on this
  currency.call(this, value, {
    errorOnInvalid: true, // otherwise, invalid values will be 0.00
    formatWithSymbol: true,
    symbol: '€',
    separator: '.',
    decimal: ',',
    pattern: '!#',
    negativePattern: '-!#',
    ...options,
  })
}
// Currency('0,1')

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
Currency.prototype = currency.prototype // enables check: 'x instanceof Currency'
