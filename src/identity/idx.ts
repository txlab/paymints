import { EthereumAuthProvider, SelfID } from '@self.id/web'

export async function createSelfID () {
  const wallet = (window as any).ethereum
  // Check if there is an injected `window.ethereum` provider
  if (!wallet) {
    throw new Error('No window.ethereum')
  }
  const addresses = await wallet.request({ method: 'eth_requestAccounts' })

  return await SelfID.authenticate({
    authProvider: new EthereumAuthProvider(wallet, addresses[0] as string),
    ceramic: 'testnet-clay',
    // Make sure the `ceramic` and `connectNetwork` parameter connect to the same network
    connectNetwork: 'testnet-clay',
  })
}

export async function setBasicProfile (selfID: SelfID) {
  // Use the SelfID instance created by the `createSelfID()` function
  await selfID.set('basicProfile', { name: 'Alice' })
}
