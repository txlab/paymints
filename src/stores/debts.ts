import { db } from './../boot/dexie'
import { liveQuery } from 'dexie'
import { useObservable } from '@vueuse/rxjs'

export type Debt = {
  id?: string
  contact: string
  amount: number
  date: Date
}

export const useDebts = () => useObservable<Debt[]>(
  // eslint-disable-next-line
  (liveQuery(() => db.debts.toArray()) as any)
)

export async function addDebt (debt:Debt) {
  return await db.debts.add(debt)
}

// export const getDebts = async () => await db.debts.toArray()
