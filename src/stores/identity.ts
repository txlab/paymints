import { defineStore } from 'pinia'

export const useIdentityStore = defineStore('identity', {
  state: () => ({
    initialized: false,
    privKey: null as string|null,
  }),
  getters: {
    // initialized: state => !!state.openlogin,
    isAuthenticated: state => !!state.privKey,
  },
  actions: {
    async init () {
      // const { initOpenLogin } = await import('src/identity/openlogin')
      // console.log('Initializing openlogin')
      // const openlogin = await initOpenLogin()
      // console.log('Initialized openlogin:', openlogin)
      // // this.privKey = openlogin.privKey
      this.initialized = true
      // return openlogin
    },
  },
})
