import { createPinia } from 'pinia'
import { boot } from 'quasar/wrappers'

// https://pinia.vuejs.org/getting-started.html#installation

export default boot(({ app }) => {
  app.use(createPinia())
})
