// import { boot } from 'quasar/wrappers'
// import {
//   createRxDatabase,
//   addRxPlugin,
// } from 'rxdb'
// import { getRxStoragePouch, addPouchPlugin } from 'rxdb/plugins/pouchdb'
// import { RxDBDevModePlugin } from 'rxdb/plugins/dev-mode'
// import VueRx from 'vue-rx'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
// export default boot(async () => {
//   if (process.env.NODE_ENV !== 'production') {
//     // in dev-mode we add the dev-mode plugin
//     // which does many checks and adds full error messages
//     addRxPlugin((await import('rxdb/plugins/dev-mode')).RxDBDevModePlugin)
//   }
//   addPouchPlugin(await import('pouchdb-adapter-idb'))
//   const db = await createRxDatabase({
//     name: 'main', // <- name
//     storage: getRxStoragePouch('idb'), // <- RxStorage
//     password: 'myPassword', // <- password (optional)
//     multiInstance: true, // <- multiInstance (optional, default: true)
//     eventReduce: true, // <- eventReduce (optional, default: true)
//   })

//   app.use(VueRx, Rx)
// })
