import { Debt } from './../stores/debts'
import Dexie, { Table } from 'dexie'
// import 'dexie-observable'

export class DB extends Dexie {
  debts!: Table<Debt, number>

  async init () {
    console.log('DB initialization')
  }

  constructor () {
    super('main')

    this.version(1).stores({
      debts: '++id, contact, time', // Primary key and indexed props
    })

    // TODO consider https://dexie.org/docs/Table/Table.defineClass()
    // this.debts.defineClass({
    // })
    // TODO or:
    // this.debts.mapToClass(Debt)

    void this.init()
  }
}

export const db = new DB()
