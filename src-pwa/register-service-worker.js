import { register } from 'register-service-worker'
import { Notify } from 'quasar'

// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  ready (registration) {
    console.log('Service worker is active.', registration)
    // Notify.create({
    //   message: 'PWA ready',
    // })
  },

  registered (registration) {
    console.log('Service worker has been registered.', registration)

    if (process.env.NODE_ENV !== 'development') {
      setInterval(() => {
        console.debug("Checking if service worker should update")
        registration.update()
          .catch(console.error)
      }, 1000 * 60)
    }
  },

  installed (registration) {
    console.log('Service worker has been installed:', registration)
    Notify.create({
      message: 'PWA Installed',
    })
  },

  cached (registration) {
    console.log('Content has been cached for offline use.', registration)
    Notify.create({
      message: 'Ready for offline',
    })
  },

  updatefound (registration) {
    console.log('New content is downloading.', registration)
  },

  updated (registration) {
    console.log('New content is available; please refresh.', registration)

    Notify.create({
      message: 'New version available',
      caption: 'Refresh to update?',
      color: 'primary',
      timeout: 0,
      // avatar: 'https://cdn.quasar.dev/img/boy-avatar.png',
      actions: [
        {
          label: 'Update',
          color: 'white',
          handler: () => {
            console.log('Updated content, reloading')
            window.location.reload(true)
            // registration.update()
            // .then(() => {
            //   })
            //   .catch(err => console.error('Failed to update', err))
          },
        },
        { label: 'Dismiss', color: 'white' },
      ],
    })
  },

  offline () {
    console.log('No internet connection found. App is running in offline mode.')
  },

  error (err) {
    console.error('Error during service worker registration:', err)
  },
})
