export default {
  preflight: false, // breaks quasar styling
  attributify: { // mega sexy https://windicss.org/integrations/vite.html#attributify-mode
    prefix: 'w:',
  },
}
